<?php
    $books = [
        [
            "name" => "The life is what you make it",
            "author" => "Preeti Shenoy",
            "year"=> "2006"
        ],
        [
            "name" => "The Subtle Art of Not Giving a Fuck",
            "author" => "Mark Manson",
            "year" => "2018"
        ],
        [
            "name" => "Anonymous book",
            "author" => "Anonymous Man",
            "year" => "2020"
        ],
        [
            "name" => "The Second Anonymous book",
            "author" => "Anonymous women",
            "year" => "2021"
        ]
    ];
    
    $fileredItems = array_filter($books, function ($book){
        return $book['year'] < '2020';
    });
    
    foreach($fileredItems as $book){
        echo $book['name'] . "(" . $book['year'] . ")" . " - " . $book["author"] . "\n";
    }
?>
